export const createGrid = (rows, columns) => {
  let grid = [];

  for (let y = 0; y < rows.length; y++) {
    for (let x = 0; x < columns.length; x++) {
      grid.push({
        name: columns[x].concat((y + 1).toString()),
        x: x,
        y: y,
        hasShip: false
      })
    }
  }

  return grid;
}

export function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.round(Math.random() * (max - min) + min);
}

export const getTilesToCheckHorizontal = (tiles, ship, xStartPosition,
                                          yStartPosition) => {
  return tiles.filter(
    (tile) => tile.x >= xStartPosition - 1 && tile.x <= xStartPosition + ship
      && tile.y >= yStartPosition - 1 && tile.y <= yStartPosition + 1);
}

export const getTilesToCheckVertical = (tiles, ship, xStartPosition,
                                        yStartPosition) => {
  return tiles.filter(
    (tile) => tile.x >= xStartPosition - 1 && tile.x <= xStartPosition + 1
      && tile.y >= yStartPosition - 1 && tile.y <= yStartPosition + ship);
}

export const isAllowed = (tiles, ship, xStartPosition, yStartPosition,
                          horizontal) => {
  const tilesToCheck = horizontal ? getTilesToCheckHorizontal(tiles, ship,
    xStartPosition, yStartPosition) : getTilesToCheckVertical(tiles, ship,
    xStartPosition, yStartPosition);
  return tilesToCheck.filter(tile => tile.hasShip === true).length === 0;
}
