import React, {useState, useEffect} from 'react';
import {createGrid, getRandomInt, isAllowed} from './helperfunctions.js';

function Grid(props) {
  const columns = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];
  const rows = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
  const availableShips = [6, 4, 4, 3, 3, 3, 2, 2, 2, 2];

  const placeShips = () => {
    console.log("placeShips")
    let tiles = createGrid(rows, columns);

    let successCounter = 0;

    availableShips.forEach(ship => {

      let squaresCovered;
      let xStartPosition;
      let yStartPosition;
      let horizontal
      let max = 10 - ship;
      let counter = 100;

      do {
        horizontal = Math.random() < 0.5;

        if (horizontal) {
          xStartPosition = getRandomInt(0, max);
          yStartPosition = getRandomInt(0, 9);
          let row = tiles.filter(tile => tile.y === yStartPosition);
          squaresCovered = row.filter(
            tile => tile.x >= xStartPosition && tile.x < xStartPosition + ship);
        } else {
          xStartPosition = getRandomInt(0, 9);
          yStartPosition = getRandomInt(0, max);
          let column = tiles.filter(tile => tile.x === xStartPosition);
          squaresCovered = column.filter(
            tile => tile.y >= yStartPosition && tile.y < yStartPosition + ship);
        }
      } while (--counter > 0 && !isAllowed(tiles, ship, xStartPosition,
        yStartPosition, horizontal))
      if (counter === 0) {
        placeShips(); // recursive by design
      } else {
        squaresCovered.map(
          square => tiles.find(
            tile => tile.name === square.name).hasShip = true);
        successCounter++;
      }
    })
    if (successCounter === availableShips.length) {
      inputState[1]({
        tiles: tiles,
        hitsToGo: inputState[0].hitsToGo,
      });
    }
  }

  const inputState = useState({tiles: [], hitsToGo: 31});

  useEffect(placeShips, []);

  const determineStyleClass =
    (tile) => {
      if (!tile.clicked) {
        return 'tile';
      } else {
        if (tile.hasShip) {
          return 'hit';
        } else {
          return 'miss';
        }
      }
    }

  const onclick = (tile) => {
    let turn;
    let tiles = inputState[0].tiles;
    let hitsToGo = inputState[0].hitsToGo

    tiles.find(square => square.name
      === tile.name).clicked = true;

    if (tile.hasShip) {
      hitsToGo--
    }

    turn = props.parentInputState[0].turn === 1 ? 2 : 1;

    inputState[1]({
      tiles: inputState[0].tiles,
      hitsToGo: hitsToGo,
    })

    setTimeout(() => {
      if (hitsToGo > 0) {
        props.parentInputState[1]({turn: turn});
      }
    }, 300);
  }

  return (
    <div>
      <h1>{`Player ${props.parentInputState[0].turn}`}</h1>
      <div className='grid'>
        {inputState[0].tiles.map(tile => <div key={tile.name}
                                              className={determineStyleClass(tile)}
                                              onClick={() => onclick(tile)}/>)}
      </div>
      <p>{`Hits to go ${inputState[0].hitsToGo}`}</p>
      {inputState[0].hitsToGo === 0 && <h1 style={{color: "red"}}>You won!</h1>}
    </div>
  );
}

export default (Grid);

