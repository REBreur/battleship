import Grid from './grid'
import './App.css';
import {useState} from "react";

function App() {
  const parentInputState = useState({turn: 1});

  return (
    <div>
      <div hidden={parentInputState[0].turn ===2}>
        <Grid parentInputState={parentInputState}/>
      </div>
      <div hidden={parentInputState[0].turn ===1}>
        <Grid parentInputState={parentInputState}/>
      </div>
    </div>
  );
};

export default App;
